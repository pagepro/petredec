
{% macro featuredTeaser(item) %}
<div class="c-featured-teaser-wrapper">
    <div class="c-featured-teaser">
        <div class="c-featured-teaser__media" style="background-image: url('static/img/bg-hero-3.jpg');"></div>
        <div class="c-arrow-wrapper c-featured-teaser__card">
            <div class="c-featured-teaser__content">
                <div class="c-featured-teaser__category">
                    {{- item.category -}}
                </div>
                <div class="c-featured-teaser__heading">
                    {{- item.heading -}}
                </div>
                <a href="#" data-video-url="/petredec_video.mp4" class="c-featured-teaser__action js-video-popup-toggler">
                    <span class="ui-icon-wrapper">
                        <svg class="ui-icon ui-icon--ico-play">
                            <use xlink:href="./static/symbol/svg/sprite.symbol.svg#ico-play">
                            </use>
                        </svg>
                    </span>
                    <span class="c-label">
                        Watch the video
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
{% endmacro %}
