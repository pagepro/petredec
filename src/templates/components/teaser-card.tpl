<div class="swiper-container c-slider-teaser-wrapper js-slider-teaser">
    <ul class="swiper-wrapper c-slider-teaser"> <!-- c-simple-list c-simple-list--col-3-->
        {% for item in [
            {
                heading: 'Trade',
                icon: 'ico-trade',
                color: 'ui-color--blue',
                href: '#',
                imgSrc: 'img-thumbnail-1.jpg'
            },
            {
                heading: 'Transport',
                icon: 'ico-transport',
                color: 'ui-color--accent',
                href: '#',
                imgSrc: 'img-thumbnail-2.jpg'
            },
            {
                heading: 'Distribute',
                icon: 'ico-distribute',
                color: 'ui-color--main',
                href: '#',
                imgSrc: 'img-thumbnail-3.jpg'
            }
        ] %}


            <li class="c-slider-teaser__item c-teaser-card-wrapper swiper-slide">
                <a href="#" class="c-teaser-card c-arrow-wrapper">
                    <div class="c-teaser-card__content {{ item.color }}">
                        <div class="c-teaser-card__icon">

                            <span class="ui-icon-wrapper">
                                <svg class="ui-icon ui-icon--{{ item.icon }}">
                                    <use xlink:href="./static/symbol/svg/sprite.symbol.svg#{{ item.icon }}">
                                    </use>
                                </svg>
                            </span>
                        </div>
                        <div class="c-teaser-card__title">
                            <h3 class="c-label">
                                {{ item.heading }}
                            </h3>
                            <span class="c-btn__arrow">
                                <span class="c-arrow">
                                    <span class="ui-icon-wrapper">
                                        <svg class="ui-icon ui-icon--ico-arrow">
                                            <use xlink:href="./static/symbol/svg/sprite.symbol.svg#ico-arrow">
                                            </use>
                                        </svg>
                                    </span>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="c-teaser-card__media">
                        <div class="c-teaser-card__img"
                            style="background-image: url('static/img/{{ item.imgSrc }}');">
                        </div>
                    </div>
                </a>
            </li>
        {% endfor %}
    </ul>
</div>