{% macro render(_active_no) %}
    {% if _active_no == '' %}
        {% set logo_href='#' %}
    {% else %}
        {% set logo_href='index.html' %}
    {% endif %}
        <header class="l-header js-sticky-header">
            <div class="l-header-navbar">
                <div class="l-inner l-header__inner">
                    <div class="l-header__logo">
                        <a href="templates" class="c-header-logo">
                            <span class="ui-icon-wrapper">
                                <svg class="ui-icon ui-icon--logo-petredec-titled">
                                    <use xlink:href="./static/symbol/svg/sprite.symbol.svg#logo-petredec-titled">
                                    </use>
                                </svg>
                            </span>
                        </a>
                    </div>
                    <nav class="l-header__nav">
                        <div class="c-header-nav-wrapper">
                            <div class="c-header-nav-logo">
                                <span class="ui-icon-wrapper">
                                    <svg class="ui-icon ui-icon--logo-petredec">
                                        <use xlink:href="./static/symbol/svg/sprite.symbol.svg#logo-petredec">
                                        </use>
                                    </svg>
                                </span>
                            </div>
                            <ul class="c-header-nav">
                                {% for link in [
                                    { href: "#", text: "Home" },
                                    { href: "#", text: "Who we are" },
                                    { href: "#", text: "What we do" },
                                    { href: "#", text: "News" },
                                    { href: "#", text: "Contact" }
                                    ] %}
                                    <li class="c-header-nav__item">
                                        <a class="c-header-nav__link" href="{{ link.href }}">
                                            <span class="c-label">
                                                {{ link.text }}
                                            </span>
                                        </a>
                                    </li>
                                {% endfor %}
                            </ul>
                            <div class="c-header-nav-contact">
                                <div class="c-header-nav-contact__toggler">
                                    <span class="ui-icon-wrapper">
                                        <svg class="ui-icon ui-icon--phone">
                                            <use xlink:href="./static/symbol/svg/sprite.symbol.svg#phone">
                                            </use>
                                        </svg>
                                    </span>
                                </div>
                                <div class="c-header-nav-contact__content">
                                    <div class="c-header-nav-contact__desc">
                                        <address class="c-header-nav-contact__address">
                                            The Leadenhall Building<br>
                                            122 Leadenhall Street<br>
                                            London EC3V 4AB
                                        </address>
                                    </div>
                                    <div class="c-header-nav-contact__action">
                                        <div class="c-header-nav-contact__btn">
                                            <a href="#" class="c-btn c-btn--inverted">
                                                <span class="c-btn__label">
                                                    Call
                                                </span>
                                            </a>
                                        </div>
                                        <div class="c-header-nav-contact__btn">
                                            <a href="#" class="c-btn">
                                                <span class="c-btn__label">
                                                    Email Us
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="l-header__nav-opener">
                        <div class="c-header-nav-opener js-menu-opener">
                            <div class="c-header-nav-opener__btn">
                                <span class="ui-icon-wrapper">
                                    <svg class="ui-icon ui-icon--grid-menu">
                                        <use xlink:href="./static/symbol/svg/sprite.symbol.svg#grid-menu">
                                        </use>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div> 
                    <div class="l-header__nav-closer">
                        <div class="c-header-nav-closer js-menu-closer">
                            <div class="c-header-nav-closer__btn">
                                <span class="ui-icon-wrapper">
                                    <svg class="ui-icon ui-icon--close">
                                        <use xlink:href="./static/symbol/svg/sprite.symbol.svg#close">
                                        </use>
                                    </svg>
                                </span>
                            </div>
                            <div class="c-header-nav-closer__text-wrapper">
                                <div class="c-header-nav-closer__text">
                                    Close
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
{% endmacro %}