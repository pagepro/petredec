import $ from 'jquery'

const $header = $('.js-sticky-header')
const gutter = 8

const StickyHeader = {
  init: function () {
    const stickyClass = 'is-sticky'
    const $window = $(window)

    $window.on('scroll', function () {
      const distance = $window.scrollTop()
      $header.toggleClass(stickyClass, distance > gutter)
    })
  }
}

StickyHeader.init()
