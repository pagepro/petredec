const defaultSliderConfig = {
  autoplay: {
    delay: 3600,
    disableOnInteraction: false
  },
  speed: 400,
  watchSlidesProgress: true
}

const sliders = [
  {
    selector: '.js-slider-teaser',
    customSliderConfig: {
      autoplay: false,
      breakpoints: {
        320: {
          slidesPerView: 1.4,
          spaceBetween: 16
        },
        480: {
          slidesPerView: 2.4,
          spaceBetween: 16
        },
        768: {
          shortSwipes: false,
          longSwipes: false,
          allowTouchMove: false,
          slidesPerView: 3,
          spaceBetween: 32,
          noSwiping: true
        }
      }
    }
  },
  {
    selector: '.js-slider-hero',
    customSliderConfig: {
      watchSlidesProgress: true,
      effect: 'fade',
      pagination: {
        el: '.js-slider-bullets',
        bulletClass: 'c-slider-bullet',
        bulletActiveClass: 'is-active',
        clickable: true,
        renderBullet: function (index, className) {
          return `
            <span class="${className} c-slider-bullet"></span>
          `
        }
      }
    }
  }
]

export {
  defaultSliderConfig,
  sliders
}
