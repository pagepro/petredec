import $ from 'jquery'
import Swiper from 'swiper/js/swiper'

import { defaultSliderConfig, sliders } from '../configs/sliders'

const cssSelectors = {
  paginationBullets: '.js-slider-bullets',
  paginationNumberFirst: '.js-slider-number-first',
  paginationNumberLast: '.js-slider-number-last',
  btnPrev: '.js-slider-btn-prev',
  btnNext: '.js-slider-btn-next',
  active: 'is-active'
}

const globalSliders = {
  init: function () {
    sliders.forEach(currentSlider => {
      const $sliderContainer = $(currentSlider.selector)

      const $sliderPaginationBullets = $sliderContainer.find(cssSelectors.paginationBullets)
      const $sliderPaginationNumberFirst = $sliderContainer.find(cssSelectors.paginationNumberFirst)
      const $sliderPaginationNumberLast = $sliderContainer.find(cssSelectors.paginationNumberLast)
      const $sliderBtnPrev = $sliderContainer.find(cssSelectors.btnPrev)
      const $sliderBtnNext = $sliderContainer.find(cssSelectors.btnNext)

      if ($sliderContainer.length) {
        const mainSlider = {
          init: function () {
            const myCurrentSlider = new Swiper($sliderContainer, {
              ...defaultSliderConfig,
              ...currentSlider.customSliderConfig,
              navigation: {
                prevEl: $sliderBtnPrev,
                nextEl: $sliderBtnNext
              },
              on: {
                init: function () {
                  // Slider Pagination Counter
                  if ($sliderPaginationNumberFirst.length) {
                    $sliderPaginationNumberFirst.text('01')
                  }

                  if ($sliderPaginationNumberLast.length) {
                    const $sliderCurrentPage = `0${this.slides.length}`
                    $sliderPaginationNumberLast.text($sliderCurrentPage)
                  }

                  // Slider Pagination Bullet
                  if ($sliderPaginationBullets.length) {
                    $sliderPaginationBullets
                      .eq(0)
                      .addClass(cssSelectors.active)
                  }
                },
                slideChange: function () {
                  console.log('slideChange')

                  // Slider Pagination Bullet
                  if ($sliderPaginationBullets.length) {
                    $sliderPaginationBullets.removeClass(cssSelectors.active)
                    $sliderPaginationBullets
                      .eq(myCurrentSlider.activeIndex)
                      .addClass(cssSelectors.active)
                  }
                }
              }
            })
          }
        }

        mainSlider.init()
      }
    })
  }
}

globalSliders.init()

export { globalSliders }
