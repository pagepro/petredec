{% macro render() %}

<footer class="l-footer
    ui-bg--dark-blue">
    <div class="l-inner l-footer__inner">
        <div class="l-footer__aside">
            <div class="l-footer__logo">
                <span class="ui-icon-wrapper">
                    <svg class="ui-icon ui-icon--logo-petredec">
                        <use xlink:href="./static/symbol/svg/sprite.symbol.svg#logo-petredec-alt">
                        </use>
                    </svg>
                </span>
            </div>
            <div class="l-footer__contact">
                <!-- c-contact-teaser -->
                    <div class="c-contact-teaser">
                        <div class="c-contact-teaser__heading">
                            <h3 class="t-typo-1 ui-color--white">
                                Get in touch
                            </h3>
                        </div>
                        <div class="c-contact-teaser__form ui-form">
                            <button class="c-btn c-btn--inverted c-btn--inverted-blue" type="submit">
                                <span class="c-btn__label">
                                    Call
                                </span>
                            </button>
                            <a href="mailto:testmail@mail.com" class="c-btn c-btn--inverted-black">
                                <span class="c-btn__label">
                                    Email us
                                </span>
                            </a>
                        </div>
                    </div>
                <!-- c-contact-teaser -->
            </div>
            <div class="l-footer__copyright">
                <h6 class="t-typo-5 ui-color--white">
                    &copy;2020 Petredec Pte Limited
                </h6>
            </div>
        </div>
        <div class="l-footer__main">
            <div class="c-footer-menu-wrapper">
                {% for menu in [
                    {
                        heading: "Company",
                        links: [
                            { href: "#", text: "Trade" },
                            { href: "#", text: "Transport" },
                            { href: "#", text: "Distribute" },
                            { href: "#", text: "News" },
                            { href: "#", text: "About us" },
                            { href: "#", text: "Contact" },
                            { href: "#", text: "Careers" }
                        ]
                    },
                    {
                        heading: "Other",
                        links: [
                            { href: "#", text: "Terms of Use" },
                            { href: "#", text: "Communication Terms" },
                            { href: "#", text: "Privacy Policy" },
                            { href: "#", text: "Tax Strategy" },
                            { href: "#", text: "Modern Slavery Statement" }
                        ]
                    }
                ] %}
                    <nav class="c-footer-menu">
                        <div class="c-footer-menu__heading">
                            {{ menu.heading }}
                        </div>
                        <ul class="c-footer-menu__list">
                            {% for link in menu.links %}
                            <li class="c-footer-menu__item">
                                <a href="{{ link.href }}" class="c-footer-menu__link">
                                    {{ link.text }}
                                </a>
                            </li>
                            {% endfor %}
                        </ul>
                    </nav>
                {% endfor %}
            </div>
        </div>
    </div>
</footer>
{% endmacro %}
