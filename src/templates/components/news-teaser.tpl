{% macro newsTeaser(item) %}
<div class="c-news-teaser-wrapper">
    <a href="{{ item.href }}" class="c-arrow-wrapper c-news-teaser {{ item.modifierClass }}">
        <div class="c-news-teaser__media">
            <div class="c-news-teaser__img" style="background-image: url('static/img/{{ item.imgSrc }}');"></div>
        </div>
        <div class="c-news-teaser__content">
            <div class="c-news-teaser__date">
                {{- item.date | safe -}}
            </div>
            <h3 class="c-news-teaser__heading">
                {{- item.heading -}}
            </h3>
            <div class="c-news-teaser__action">
                <span class="c-arrow">
                    <span class="ui-icon-wrapper">
                        <svg class="ui-icon ui-icon--ico-arrow ui-color--white">
                            <use xlink:href="./static/symbol/svg/sprite.symbol.svg#ico-arrow">
                            </use>
                        </svg>
                    </span>
                </span>
            </div>
        </div>
    </a>
</div>
{% endmacro %}
