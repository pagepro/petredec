yourfilenames=`ls *.html`;
outputHtml="";
stringContent="";
stringPre="
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>
        <meta charset=\"utf-8\">
        <title></title>
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width\">

        <link rel=\"stylesheet\" href=\"main.css\">
        <script type=\"text/javascript\" src=\"//use.typekit.net/wcn8fgi.js\"></script>
        <script type=\"text/javascript\">try{Typekit.load();}catch(e){}</script>

        <!--[if lt IE 9]>
            <script src=\"//html5shiv.googlecode.com/svn/trunk/html5.js\"></script>
            <script>window.html5 || document.write('<script src=\"js/vendor/html5shiv.js\"><\/script>')</script>
        <![endif]-->
    </head>
    <body>

        <div class=\"wrapper\">
            <div class=\"content\">
                <h1>
                  Petredec
                </h1>
                <h2>Templates</h2>
                <ul>
";

for eachfile in $yourfilenames
do
  stringContent+="<li><a href=\"../"${eachfile}"\">${eachfile}</a></li>"
done

stringPost="
                </ul>
                <p class=\"copy\">
                    &copy; 2020<br>
                    All Rights Reserved.
                </p>
            </div>
        </div>
    </body>
</html>
";




outputHtml+=$stringPre
outputHtml+=$stringContent
outputHtml+=$stringPost

echo $outputHtml > "templates/index.html"
