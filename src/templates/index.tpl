{% extends "base/base.tpl" %}
{% block title %}
    Homepage
{% endblock %}
{% block content %}


<section class="l-hero">
    {# {% include "components/hero-slider.tpl" %} #}
    {% include "components/hero-video.tpl" %}
</section>

<section class="ui-bg--gradient-grey
    l-sec
    l-sec--gutter-top-big
    l-sec--gutter-top-big-mobile
    l-sec--gutter-bottom-big
    l-sec--gutter-bottom-big-mobile">
    <div class="l-inner">
        {% include "components/teaser-card.tpl" %}
    </div>
</section>

<section class="l-sec
    l-sec--gutter-top-big
    l-sec--gutter-top-big-mobile
    l-sec--gutter-bottom-big
    l-sec--gutter-bottom-big-mobile">
    <div class="l-inner">
        {% include "components/news.tpl" %}
    </div>
</section>


{% from 'components/featured-teaser.tpl' import featuredTeaser %}
{% from 'components/post-teaser.tpl' import postTeaser %}
<div class="l-sec
    l-sec--gutter-bottom-big
    l-sec--gutter-bottom-big-mobile
    l-sec--video-teaser
    ui-bg--gradient-grey">
    <div class="l-inner">
        {{
            featuredTeaser(
                item = {
                    category: 'featured',
                    heading: 'Heading example'
                }
            )
        }}
        <ul class="c-simple-list c-simple-list--col-2">
            <li class="c-simple-list__item">
                {{
                    postTeaser(
                        item = {
                            heading: 'Transport',
                            desc: 'With over 30 owned vessels and consistently in excess of 60 vessels including those on time charter, Petredec is one of the world’s largest ship owners and charterers of LPG vessels.',
                            href: '#'
                        }
                    )
                }}
            </li>
            <li class="c-simple-list__item">
                {{
                    postTeaser(
                        item = {
                            heading: 'Transport',
                            desc: 'With over 30 owned vessels and consistently in excess of 60 vessels including those on time charter, Petredec is one of the world’s largest ship owners and charterers of LPG vessels.',
                            href: '#'
                        }
                    )
                }}
            </li>
        </ul>
    </div>
</div>

{% endblock %}
