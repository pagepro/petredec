const $videoElement = document.querySelectorAll('.js-video')

document.addEventListener('DOMContentLoaded', () => {
  for (let i = 0; i < $videoElement.length; i++) {
    $videoElement[i].setAttribute('playsinline', '')
  }
})
