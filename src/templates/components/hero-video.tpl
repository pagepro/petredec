<div class="c-hero-slider-wrapper">
    <div class="c-hero-video-wrapper">
        <div class="c-hero-video">
            <div class="c-hero-video__mask"></div>
            <div class="c-hero-video__bg">
                <video class="js-video" poster="./static/img/bg-hero-1.jpg" autoplay loop muted plays-inline>
                    <source src="petredec_video.mp4" type="video/mp4">
                </video>
            </div>
            <div class="c-hero-video__content">
                <div class="c-hero-slide">
                    <h2 class="c-hero-slide__heading c-hero-slide__heading--alt">
                        <span class="c-mark">
                            We are
                        </span>
                        <br>
                        fuelling
                        <br>
                        progress
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>
