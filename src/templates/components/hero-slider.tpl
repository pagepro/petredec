<div class="swiper-container c-hero-slider-wrapper js-slider-hero">
    <div class="c-slider-pagination js-slider-pagination-counter">
        <div class="c-slider-pagination__slide-number js-slider-number-first"></div>
        <div class="c-slider-pagination__bullets js-slider-bullets"></div>
        <div class="c-slider-pagination__slide-number js-slider-number-last"></div>
    </div>
    <ul class="swiper-wrapper c-hero-slider">
        {% for item in [
            {
                text: 'We are<br><span class="c-mark">fuelling<br>progress</span>',
                imgSrc: 'bg-hero-1.jpg',
                imgAlt: 'Image'
            },
            {
                text: 'We are<br><span class="c-mark">progress</span>',
                imgSrc: 'bg-hero-2.jpg',
                imgAlt: 'Image'
            },
            {
                text: '<span class="c-mark">We are</span><br>fuelling<br>progress',
                imgSrc: 'bg-hero-2.jpg',
                imgAlt: 'Image'
            }
        ] %}
            <li class="c-hero-slide-wrapper swiper-slide">
                <div class="c-hero-slide">
                    <div class="c-hero-slide__bg" style="background-image: url('static/img/{{ item.imgSrc }}');"></div>
                    <h2 class="c-hero-slide__heading ui-anim-slide-up">
                        {{- item.text | safe -}}
                    </h2>
                </div>
            </li>
        {% endfor %}
    </ul>
</div>