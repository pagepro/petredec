import $ from 'jquery'

const cssSelectors = {
  btnOpener: '.js-menu-opener',
  btnCloser: '.js-menu-closer'
}

const cssClasses = {
  desktop: 'is-desktop',
  mobile: 'is-mobile',
  isOpened: 'is-opened',
  hasOpenMenu: 'has-open-menu'
}

const Menu = {
  $window: null,
  $body: null,
  init: function () {
    this.$window = $(window)
    this.$body = $('body')
    this.menuHandlers()
    this.resizeWatch()
  },
  menuHandlers () {
    $(cssSelectors.btnOpener).on('click', e => {
      e.preventDefault()
      this.$body.addClass(cssClasses.hasOpenMenu)
    })
    $(cssSelectors.btnCloser).on('click', e => {
      e.preventDefault()
      this.$body.removeClass(cssClasses.hasOpenMenu)
    })
  },
  resizeWatch () {
    this.$window.on('resize.headerMenu orientationchange.headerMenu', () => {
      this.$body.removeClass(cssClasses.hasOpenMenu)
    }).trigger('resize.headerMenu')
  }
}

Menu.init()
