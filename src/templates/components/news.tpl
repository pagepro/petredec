{% from '../components/news-teaser.tpl' import newsTeaser %}

<div class="c-news-wrapper">
    <div class="c-news">
        <div class="c-news__heading">
            Latest news
        </div>
        <ul class="c-news__list">

            {% for item in [
                {
                    modifierClass: " c-news-teaser--big ",
                    heading: 'What will “business as usual” look like in your industry tomorrow?',
                    date: '<time datetime="2020-01-17">17 January, 2020</time>',
                    href: '#',
                    imgSrc: 'img-thumbnail-1.jpg'
                },
                {
                    heading: 'What will “business as usual” look like in your industry tomorrow?',
                    date: '<time datetime="2020-01-18">18 January, 2020</time>',
                    href: '#',
                    imgSrc: 'img-thumbnail-1.jpg'
                },
                {
                    heading: 'What will “business as usual” look like in your industry tomorrow?',
                    date: '<time datetime="2020-01-19">19 January, 2020</time>',
                    href: '#',
                    imgSrc: 'img-thumbnail-1.jpg'
                }
            ] %}
                <li class="c-news__item">

                    <!-- c-news-teaser -->
                        {{ newsTeaser(item) }}
                    <!-- c-news-teaser -->

                </li>
            {% endfor %}
        </ul>

        <div class="c-news__btn">
            <a href="#" class="c-arrow-wrapper c-btn c-btn--huge c-btn--orange">
                <span class="c-btn__label">
                    Read more on our blog
                </span>
                <span class="c-btn__arrow">
                    <span class="c-arrow">
                        <span class="ui-icon-wrapper">
                            <svg class="ui-icon ui-icon--ico-arrow ui-color--white">
                                <use xlink:href="./static/symbol/svg/sprite.symbol.svg#ico-arrow">
                                </use>
                            </svg>
                        </span>
                    </span>
                </span>
            </a>
        </div>
    </div>
</div>
