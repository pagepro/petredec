import $ from 'jquery'

import videoModalTemplate from './templates/video-modal.handlebars'

const cssSelectors = {
  videoModalCloseBtn: '.js-video-popup-cls-btn',
  videoModalTrigger: '.js-video-popup-toggler',
  videoModalOverlay: '.js-modal-wrap'
}

const css = {
  modalActive: 'is-modal-active',
  searchBarOpened: 'is-watch-popup-active'
}

const defaultVideoSrc = ''

const videoPopup = {
  init: function () {
    const $body = $('body')
    const $videoModalTrigger = $(cssSelectors.videoModalTrigger)

    if (!$videoModalTrigger) return

    const videoSrc = $videoModalTrigger.attr('data-video-url')

    $videoModalTrigger.on('click', function (e) {
      e.preventDefault()

      if (!$('.l-modal.l-modal--video').length) {
        const newModal = $(`
          <div class="l-modal l-modal--video js-modal-wrap">
            <div class="l-modal__overlay js-modal-overlay"></div>
            <div class="l-modal__content"></div>
            <div class="l-modal__btn-close js-video-popup-cls-btn">
              <span class="ui-icon-wrapper">
                <svg class="ui-icon ui-icon--close">
                    <use xlink:href="./static/symbol/svg/sprite.symbol.svg#close">
                    </use>
                </svg>
              </span>
            </div>
          </div>
        `)

        $body.append(newModal)
      }

      const $videoModalOverlay = $(cssSelectors.videoModalOverlay)
      const $videoModalCloseBtn = $(cssSelectors.videoModalCloseBtn)

      $body.addClass(css.searchBarOpened)
      $body.addClass(css.modalActive)

      const videoParams = {
        videoSrc: videoSrc || defaultVideoSrc
      }

      $('.l-modal__content').html(videoModalTemplate({ ...videoParams }))

      const currentVideo = $('.l-modal__content').find('video')[0]

      currentVideo.setAttribute('playsinline', '')
      currentVideo.play()

      $videoModalOverlay.on('click', () => { videoPopup.closeModal() })
      $videoModalCloseBtn.on('click', () => { videoPopup.closeModal() })
    })
  },
  closeModal: function () {
    const $body = $('body')

    $(cssSelectors.videoModalOverlay).remove()
    $(cssSelectors.searchBar).removeClass(css.visible)

    $body.removeClass(css.searchBarOpened)
    $body.removeClass(css.modalActive)
  }
}

videoPopup.init()
