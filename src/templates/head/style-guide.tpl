<style>
    .c-colors-wrapper {
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;
    }
    .c-color-list {
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;

        margin: 20px 0;
        padding: 0;

        list-style: none;

        flex-wrap: wrap;
    }
    .c-color-list__item {
        display: inline-block;

        width: 150px;
        height: 150px;
        margin: 3px;
        padding: 16px;
        font-size: 12px;

        transition: 170ms ease-in-out;
        text-align: left;
    }
    .c-guide-section:last-of-type {
        margin-bottom: 300px;
    }
    .c-guide-section__title {
        width: 100%;
        width: calc(100% + 2rem);
        margin: 2rem -1rem 1.5rem;
        padding: 1rem;

        font-size: 20px;

        border-radius: 1rem;
        background: #efefef;
    }
    .c-guide-section__title h2 {
        font-weight: 400;
    }
    .c-btn:not(:last-child) {
        margin-bottom: 1rem;
    }
    .c-news-box--vertical {
        width: 240px;
    }
    .c-news-box--minimal {
        width: 275px;
    }
    .c-event-box--vertical {
        width: 245px;
    }
    .c-guide-typo {
        margin: 15px 0 25px;
    }
    .c-guide-typo + .c-guide-typo {
        margin-top: 25px;
        padding-top: 25px;

        border-top: 1px solid grey;
    }
    .c-guide-typo__example {
        padding: 5px 0;
    }
    .c-guide-typo__desc {
        font-size: 12px;

        color: gray;
    }
    .c-guide-row {
        display: flex;
    }
    .l-styleguide-footer {
        padding: 0;
    }

</style>
