
{% macro postTeaser(item) %}
<div class="c-post-teaser-wrapper">
    <a href="{{ item.href }}" class="c-arrow-wrapper c-post-teaser">
        <div class="c-post-teaser__content">
            <div class="c-post-teaser__heading">
                {{- item.heading -}}
            </div>
            <div class="c-post-teaser__desc">
                {{- item.desc -}}
            </div>
            <div class="c-post-teaser__action">
                <span class="c-post-teaser__btn">
                    <span class="c-label">
                        Read more
                    </span>
                    <span class="c-btn__arrow">
                        <span class="c-arrow">
                            <span class="ui-icon-wrapper">
                                <svg class="ui-icon ui-icon--ico-arrow ui-color--main">
                                    <use xlink:href="./static/symbol/svg/sprite.symbol.svg#ico-arrow">
                                    </use>
                                </svg>
                            </span>
                        </span>
                    </span>
                </span>
            </div>
        </div>
    </a>
</div>
{% endmacro %}
