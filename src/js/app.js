import './polyfills'
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import svg4everybody from 'svg4everybody'

import './modules/video-modal'
import './modules/menu'
import './modules/sticky-header'
import './modules/slider'
import './modules/video-bg'

svg4everybody()
