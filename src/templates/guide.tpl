<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>{% block title %} {{ title }} {% endblock %}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        {% include "head/head-links.tpl" %}
        {% include "head/style-guide.tpl" %}
    </head>
    <body>
        <main class="l-main">
            <div class="l-container">
                <div class="l-inner">
                    <!-- Colors -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    Colour Pallette
                                </h2>
                            </div>
                            <div class="c-guide-section__content">
                                <ul class="c-color-list">
                                    {% for color in [
                                        { className: "main", needNegative: true },
                                        { className: "grey" },
                                        { className: "grey-dark" },
                                        { className: "grey-light" },
                                        { className: "white"},
                                        { className: "accent" },
                                        { className: "blue" },
                                        { className: "blue-dark", needNegative: true },
                                        { className: "blue-darkest", needNegative: true }
                                    ] %}
                                        <li class="c-color-list__item ui-bg--{{ color.className }} {% if color.needNegativeText %} ui-color--white {% endif %}"
                                        style=" {% if color.needNegative %} color: white; {% endif %} border: 1px solid lightgrey">
                                        <strong style="text-transform: uppercase">{{ color.className }}</strong><br><br>
                                        .ui-bg--{{ color.className }}<br>
                                        .ui-color--{{ color.className }}
                                        </li>
                                    {% endfor %}
                                </ul>
                            </div>
                        </div>
                    <!-- Colors -->
                    <!-- Headings -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    Headings
                                </h2>
                            </div>
                            <div class="c-guide-section__content">
                                {% for typo in [
                                    {
                                        tagElement: "h1",
                                        className: "t-headline-1",
                                        title: "Work Sans - Semibold - 144px",
                                        exampleText: "Heading 0"
                                    },
                                    {
                                        tagElement: "h2",
                                        className: "t-headline-2",
                                        title: "Work Sans - Semibold - 64px",
                                        exampleText: "Heading 1"
                                    },
                                    {
                                        tagElement: "h3",
                                        className: "t-headline-3",
                                        title: "Work Sans - Semibold - 48px",
                                        exampleText: "Heading 2"
                                    },
                                    {
                                        tagElement: "h4",
                                        className: "t-headline-4",
                                        title: "Work Sans - Semibold - 36px",
                                        exampleText: "Heading 3"
                                    },
                                    {
                                        tagElement: "h5",
                                        className: "t-headline-5",
                                        title: "Work Sans - Semibold - 24px",
                                        exampleText: "Heading 4"
                                    },
                                    {
                                        tagElement: "p",
                                        className: "t-typo-1",
                                        title: "Work Sans - Medium - 16px",
                                        exampleText: "Call to Action"
                                    },
                                    {
                                        tagElement: "p",
                                        className: "t-typo-2",
                                        title: "Work Sans - Medium - 24px",
                                        exampleText: "Button"
                                    },
                                    {
                                        tagElement: "p",
                                        className: "t-typo-3",
                                        title: "Roboto - Regular - 18px",
                                        exampleText: "Body Copy - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
                                    },
                                    {
                                        tagElement: "p",
                                        className: "t-typo-4",
                                        title: "Roboto - Regular - 12px",
                                        exampleText: "Info"
                                    }
                                ] %}
                                    <div class="c-guide-typo">
                                        <p class="c-guide-typo__desc">{{ typo.title }}</p>
                                        <div class="c-guide-typo__example">
                                            <{{ typo.tagElement }} class="{{ typo.className }}">
                                                {{ typo.exampleText }}
                                            </{{ typo.tagElement }}>
                                        </div>
                                        <code class="c-guide-typo__desc">
                                            .{{ typo.className }}
                                        </code>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>
                    <!-- Headings -->



                    <!-- c-contact-teaser -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    Contact-teaser
                                </h2>
                            </div>
                            <div class="c-guide-section__content" style="background: #00375D;">
                                <!-- c-contact-teaser -->
                                    <div class="c-contact-teaser">
                                        <div class="c-contact-teaser__heading">
                                            <h3 class="t-typo-1 ui-color--white">
                                                Get in touch
                                            </h3>
                                        </div>
                                        <div class="c-contact-teaser__form ui-form">
                                            <button class="c-btn c-btn--inverted c-btn--inverted-blue" type="submit">
                                                <span class="c-btn__label">
                                                    Call
                                                </span>
                                            </button>
                                            <input type="text" placeholder="Email us"/>
                                        </div>
                                    </div>
                                <!-- c-contact-teaser -->
                            </div>
                        </div>
                    <!-- c-contact-teaser -->


                    <!-- c-hero-slider -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    Hero
                                </h2>
                            </div>
                            <div class="c-guide-section__content">
                                <!-- c-hero-slider -->

                                    <div class="l-header-hero">
                                        {% include "components/hero-slider.tpl" %}
                                    </div>

                                <!-- c-hero-slider -->
                            </div>
                        </div>
                    <!-- c-hero-slider -->

                    <!-- c-hero-video -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    Hero Video
                                </h2>
                            </div>
                            <div class="c-guide-section__content">
                                <!-- c-hero-slider -->

                                    <div class="l-header-hero">
                                        {% include "components/hero-video.tpl" %}
                                    </div>

                                <!-- c-hero-slider -->
                            </div>
                        </div>
                    <!-- c-hero-slider -->

                    <!-- c-teaser-card -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    Teaser card - list
                                </h2>
                            </div>
                            <div class="c-guide-section__content" style="padding: 32px; background-color: #efefef;">
                                <!-- c-teaser-card -->

                                    {% include "components/teaser-card.tpl" %}

                                <!-- c-teaser-card -->
                            </div>
                        </div>
                    <!-- c-teaser-card -->


                    <!-- c-hero-slider -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    News Teaser
                                </h2>
                            </div>
                            <div class="c-guide-section__content">
                                <!-- c-hero-slider -->
                                    <div class="l-header-hero">
                                        {% include "components/hero-slider.tpl" %}
                                    </div>
                                <!-- c-hero-slider -->
                            </div>
                        </div>
                    <!-- c-hero-slider -->


                    <!-- c-post-teaser -->
                    <div class="c-guide-section">
                        <div class="c-guide-section__title">
                            <h2>
                                Post Teaser
                            </h2>
                        </div>
                        <div class="c-guide-section__content" style="max-width: 550px;">
                            <!-- c-post-teaser -->
                                {% from 'components/post-teaser.tpl' import postTeaser %}
                                {{
                                    postTeaser(
                                        item = {
                                            heading: 'Transport',
                                            desc: 'With over 30 owned vessels and consistently in excess of 60 vessels including those on time charter, Petredec is one of the world’s largest ship owners and charterers of LPG vessels.',
                                            href: '#'
                                        }
                                    )
                                }}
                            <!-- c-post-teaser -->
                        </div>
                    </div>
                    <!-- c-post-teaser -->


                    <!-- c-news-teaser -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    News Teaser
                                </h2>
                            </div>
                            <div class="c-guide-section__content" style="max-width: 550px;">
                                <!-- c-news-teaser -->
                                    {% from 'components/news-teaser.tpl' import newsTeaser %}
                                    {{
                                        newsTeaser(
                                            item = {
                                                heading: 'What will “business as usual” look like in your industry tomorrow?',
                                                date: '<time datetime="2020-01-17">17 January, 2020</time>',
                                                href: '#',
                                                imgSrc: 'img-thumbnail-1.jpg'
                                            }
                                        )
                                    }}
                                <!-- c-news-teaser -->
                            </div>
                        </div>
                    <!-- c-news-teaser -->


                    <!-- c-news-teaser--big -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    News Teaser - big
                                </h2>
                            </div>
                            <div class="c-guide-section__content" style="max-width: 550px;">
                                <!-- c-news-teaser--big -->
                                    {{
                                        newsTeaser(
                                            item = {
                                                modifierClass: " c-news-teaser--big ",
                                                heading: 'What will “business as usual” look like in your industry tomorrow?',
                                                date: '<time datetime="2020-01-17">17 January, 2020</time>',
                                                href: '#',
                                                imgSrc: 'img-thumbnail-1.jpg'
                                            }
                                        )
                                    }}
                                <!-- c-news-teaser -->
                            </div>
                        </div>
                    <!-- c-news-teaser -->


                    <!-- c-news -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    News List (with teasers)
                                </h2>
                            </div>
                            <div class="c-guide-section__content">
                                <!-- c-news -->
                                    {% include "components/news.tpl" %}
                                <!-- c-news -->
                            </div>
                        </div>
                    <!-- c-news -->


                    <!-- c-featured-teaser -->
                        <div class="c-guide-section">
                            <div class="c-guide-section__title">
                                <h2>
                                    Featured Teaser
                                </h2>
                            </div>
                            <div class="c-guide-section__content" style="padding: 32px; background-color: #efefef;">
                                <!-- c-featured-teaser -->
                                    {% from 'components/featured-teaser.tpl' import featuredTeaser %}
                                    {{
                                        featuredTeaser(
                                            item = {
                                                category: 'featured',
                                                heading: 'Heading example'
                                            }
                                        )
                                    }}
                                <!-- c-featured-teaser -->
                            </div>
                        </div>
                    <!-- c-featured-teaser -->


                </div>
            </div>
        </main>
        <script src="./static/js/app.js"></script>
    </body>
</html>