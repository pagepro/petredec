const BREAKPOINTS = {
  tablet: 1024,
  phone: 767
}

// Noop function for shorthand if
const noop = () => {}

// Screen resolution checkers
const isTablet = () => window.matchMedia(`(max-width: ${BREAKPOINTS.tablet}px)`).matches
const isMobile = () => window.matchMedia(`(max-width: ${BREAKPOINTS.phone}px)`).matches

module.exports = {
  BREAKPOINTS,
  noop,
  isTablet,
  isMobile
}
